﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiJWT
{
    public class RequestExcel
    {
        public IFormFile ExcelFile { get; set; }
        public int CodigoArchivo { get; set; }
    }
}
