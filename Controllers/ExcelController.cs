﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ApiJWT.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ExcelController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ProcesaExcel()
        {
            string fileName = "Ejemplo.xlsx";
            Dictionary<string, string> DatosExcel = new Dictionary<string, string>();

            using (var excelito = new XLWorkbook(fileName))
            {
                var filasusadas = excelito.Worksheet(1).RowsUsed();
                var i = 0;

                foreach (var fila in filasusadas)
                {
                    if (i > 0)
                    {
                        //var columnasusadas = excelito.Worksheet(1).ColumnsUsed();
                        //var cuantascol = columnasusadas.Count();

                        //for (var k = 1; k <= cuantascol; k++)
                        //{
                        //    DatosExcel.Add(fila.Cell(1).Value.ToString(), "");
                        //}

                        DatosExcel.Add(fila.Cell(1).Value.ToString(), fila.Cell(2).Value.ToString());
                    }
                    i++;
                }

            }

            var contadorDatos = DatosExcel.Count;

            using (var nuevoExcel = new XLWorkbook())
            {
                var hoja = nuevoExcel.Worksheets.Add("Hojita");
                var currentRow = 1;

                hoja.Cell(currentRow, 1).Value = "Estes es el ID";
                hoja.Cell(currentRow, 1).Style.Font.Bold = true;
                hoja.Cell(currentRow, 2).Value = "Este es el nombre";
                hoja.Cell(currentRow, 2).Style.Font.Bold = true;

                foreach (KeyValuePair<string, string> datoExcel in DatosExcel)
                {
                    currentRow++;
                    hoja.Cell(currentRow, 1).Value = datoExcel.Key;
                    hoja.Cell(currentRow, 2).Value = datoExcel.Value;
                }

                using (var stream = new MemoryStream())
                {
                    nuevoExcel.SaveAs(stream);
                    var contentStream = stream.ToArray();

                    return File(contentStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "MATOTUNCOTUTATA.xlsx");
                }

            }

        }
    }
}
